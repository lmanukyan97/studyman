<?php
namespace App\Exceptions;

use Exception;

class DuplicateException extends Exception
{
    public function __construct()
    {
        parent::__construct();
    }
}
