<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ClassModel;
use App\Models\Lecture;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'class_id',
    ];

    public function class(){
        return $this->belongsTo(ClassModel::class);
    }

    public function lectures(){
        return $this->belongsToMany(Lecture::class, 'student_lecture', 'student_id');
    }
}
