<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Student;
use App\Models\Lecture;

class ClassModel extends Model
{
    use HasFactory;

    protected $table = 'classes';

    protected $fillable = [
        'name'
    ];

    public function students(){
        return $this->hasMany(Student::class, 'class_id');
    }

    public function lectures(){
        return $this->belongsToMany(Lecture::class, 'class_lecture', 'class_id')->withPivot('sort_order');
    }

}
