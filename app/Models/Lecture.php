<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Student;
use App\Models\ClassModel;

class Lecture extends Model
{
    use HasFactory;

    protected $fillable = [
        'topic',
        'description',
    ];

    public function students(){
        return $this->belongsToMany(Student::class, 'student_lecture', 'lecture_id');
    }
}
