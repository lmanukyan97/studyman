<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class CreateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|unique:App\Models\Student,email',
            'name' => 'required|max:255',
            'class_id' => 'nullable|integer',
        ];
    }

    public function bodyParameters(): array
    {
        return $this->rules();
    }
}
