<?php

namespace App\Http\Requests\Lecture;

use Illuminate\Foundation\Http\FormRequest;

class CreateLectureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'topic' => 'required|unique:App\Models\Lecture,topic|max:255',
            'description' => 'string',
        ];
    }

    public function bodyParameters(): array
    {
        return $this->rules();
    }
}
