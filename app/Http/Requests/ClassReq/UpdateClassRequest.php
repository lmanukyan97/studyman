<?php

namespace App\Http\Requests\ClassReq;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:App\Models\ClassModel,name|max:255',
        ];
    }

    public function bodyParameters(): array
    {
        return $this->rules();
    }
}
