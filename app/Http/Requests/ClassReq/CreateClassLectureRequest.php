<?php

namespace App\Http\Requests\ClassReq;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\RequiredIf;

class CreateClassLectureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'class_id' => 'required|integer|min:1',
            'lecture_id' => 'required|integer|min:1',
            'sort_order' => 'nullable|integer|min:1',
        ];
    }

    public function bodyParameters(): array
    {
        return $this->rules();
    }
}
