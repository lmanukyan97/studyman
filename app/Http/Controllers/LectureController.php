<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\ServerResponse\ServerResponse;
use App\Http\Requests\Lecture\CreateLectureRequest;
use App\Http\Requests\Lecture\UpdateLectureRequest;
use App\Services\LectureService;

class LectureController extends Controller
{
    protected $lectureService;

    public function __construct(LectureService $lectureService){
        $this->lectureService = $lectureService;
    }

    public function index() : JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->lectureService->getAll();
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function show(int $id) : JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->lectureService->getById($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function create(CreateLectureRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->lectureService->create($data);
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function update(int $id, UpdateLectureRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->lectureService->update($id, $data);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function delete(int $id): JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->lectureService->delete($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

}
