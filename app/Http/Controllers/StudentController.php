<?php

namespace App\Http\Controllers;

use App\Exceptions\DuplicateException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\ServerResponse\ServerResponse;
use App\Http\Requests\Student\CreateStudentRequest;
use App\Http\Requests\Student\UpdateStudentRequest;
use App\Http\Requests\Student\CreateStudentLectureRequest;
use App\Services\StudentService;

class StudentController extends Controller
{
    protected $studentService;

    public function __construct(StudentService $studentService){
        $this->studentService = $studentService;
    }

    public function index() : JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->studentService->getAll();
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function show(int $id) : JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->studentService->getById($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function create(CreateStudentRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->studentService->create($data);
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function update(int $id, UpdateStudentRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->studentService->update($id, $data);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function delete(int $id): JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->studentService->delete($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function addLecture(CreateStudentLectureRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->studentService->addLecture($data);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(DuplicateException $e){
            $result = ServerResponse::RESPONSE_409;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }
}
