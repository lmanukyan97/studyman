<?php

namespace App\Http\Controllers;

use App\Exceptions\DuplicateException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ClassReq\CreateClassRequest;
use App\Http\Requests\ClassReq\UpdateClassRequest;
use App\Http\Requests\ClassReq\CreateClassLectureRequest;
use App\Http\Requests\ClassReq\UpdateClassLectureRequest;
use App\ServerResponse\ServerResponse;
use App\Services\ClassService;

class ClassController extends Controller
{
    protected $classService;

    public function __construct(ClassService $classService){
        $this->classService = $classService;
    }

    public function index() : JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->getAll();
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function show(int $id) : JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->getById($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function create(CreateClassRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->create($data);
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function update(int $id, UpdateClassRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->update($id, $data);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function delete(int $id): JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->delete($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function showLectures(int $id) : JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->getLectures($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function addLecture(CreateClassLectureRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->addLecture($data);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(DuplicateException $e){
            $result = ServerResponse::RESPONSE_409;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function updateLecture(int $id, UpdateClassLectureRequest $request): JsonResponse {
        $data = $request->validated();
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->updateLectureOrder($id, $data);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

    public function deleteLecture(int $id): JsonResponse {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = $this->classService->deleteLecture($id);
        } catch(ModelNotFoundException $e){
            $result = ServerResponse::RESPONSE_404;
        } catch(Exception $e){
            $result = ServerResponse::RESPONSE_500;
        }
        return response()->json($result, $result['status']);
    }

}
