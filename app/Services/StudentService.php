<?php

namespace App\Services;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use App\Exceptions\DuplicateException;
use App\Models\ClassModel;
use App\Models\Lecture;
use App\Models\Student;

class StudentService
{
    public function getAll() : Collection {
        return Student::get();
    }

    public function getById(int $id) : Student {
        return Student::findOrFail($id)->load('class', 'lectures');
    }

    public function create(array $data) : Student {
        return Student::create($data);
    }

    public function update(int $id, array $data) : Student {
        $student = Student::findOrFail($id);
        $student->update($data);
        return $student;
    }

    public function delete(int $id) : bool {
        $result = Student::findOrFail($id)->delete();
        return $result;
    }

    public function addLecture(array $data) {
        // Проверка лекции, если нет то выходим
        Lecture::findOrFail($data['lecture_id']);

        // Проверка студента, если нет то выходим
        $student = Student::findOrFail($data['student_id']);

        // Проверка дубликата лекции студента
        $duplicate = $this->getStudentLecture($data['student_id'], $data['lecture_id']);
        if( $duplicate !== null ){
            throw new DuplicateException();
        }

        $student->lectures()->attach($data['lecture_id']);

        return true;
    }

    public function getStudentLecture($student_id, $lecture_id) : ? Student {
        return Student::whereHas('lectures', function($query) use($student_id, $lecture_id) {
            $query->where('student_id', $student_id);
            $query->where('lecture_id', $lecture_id);
        })->first();
    }
}
