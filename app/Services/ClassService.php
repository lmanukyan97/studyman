<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\DuplicateException;
use App\Models\ClassModel;
use App\Models\Student;
use App\Models\Lecture;

class ClassService
{

    public function getAll() : Collection {
        return ClassModel::get();
    }

    public function getById(int $id) : ClassModel {
        return ClassModel::findOrFail($id)->load('students');
    }

    public function getByIds(array $ids) : Collection {
        return ClassModel::find($ids);
    }

    public function create(array $data) : ClassModel {
        return ClassModel::create($data);
    }

    public function update(int $id, array $data) : ClassModel {
        $class = ClassModel::findOrFail($id);
        $class->update($data);
        return $class;
    }

    public function delete(int $id) : bool {
        ClassModel::findOrFail($id)->delete();
        Student::where('class_id', $id)->update(['class_id' => 0]);
        return true;
    }

    public function getLectures(int $id) : ClassModel {
        return ClassModel::findOrFail($id)->load(['lectures' => function ($q) {
           $q->orderBy('class_lecture.sort_order','asc');
        }]);
    }

    public function addLecture(array $data): bool {
        // Проверка лекции, если нет то выходим
        Lecture::findOrFail($data['lecture_id']);

        // Проверка класса, если нет то выходим
        $class = ClassModel::findOrFail($data['class_id']);

        // Проверка дубликата лекции
        $duplicate = $this->getClassLecture($data['class_id'], $data['lecture_id']);
        if( $duplicate !== null ){
            throw new DuplicateException();
        }

        $sort_order = $this->lectureNextSortOrder($data['class_id']);

        $class->lectures()->attach($data['lecture_id'], ['sort_order' => $sort_order]);

        return true;
    }

    public function updateLectureOrder(int $id, array $data) : bool {
        $relatedClassLecture = DB::table('class_lecture')->where('id', $id);

        if( $relatedClassLecture->doesntExist() ){
            throw new ModelNotFoundException();
        }

        $classLecture = $relatedClassLecture->first();
        $next_sort_order = $this->lectureNextSortOrder($classLecture->class_id);

        if($classLecture->sort_order > $data['sort_order']){
            $sort_order = $sort_order > 0 ? $sort_order : 1;
            DB::table('class_lecture')
            ->where('class_id', $classLecture->class_id)
            ->where('sort_order', '>=', $data['sort_order'])
            ->where('sort_order', '<', $classLecture->sort_order)
            ->increment('sort_order', 1);
        } elseif( $classLecture->sort_order < $data['sort_order'] ) {
            $sort_order = ($data['sort_order'] > $next_sort_order) ? $next_sort_order : $data['sort_order'];
            DB::table('class_lecture')
            ->where('class_id', $classLecture->class_id)
            ->where('sort_order', '>', $classLecture->sort_order)
            ->where('sort_order', '<=', $data['sort_order'])
            ->decrement('sort_order', 1);
        }else{
            return true;
        }

        return $relatedClassLecture->update(['sort_order' => $sort_order]);
    }

    public function deleteLecture(int $id): bool  {
        return DB::table('class_lecture')
            ->where('id', $id)
            ->delete();
    }

    public function lectureNextSortOrder($class_id) : int {
        $sort_order = DB::table('class_lecture')
            ->where('class_id', $class_id)
            ->orderBy('sort_order', 'desc')
            ->pluck('sort_order')
            ->first();
        return $sort_order ? $sort_order + 1 : 1;
    }

    public function getClassLecture($class_id, $lecture_id) : ? ClassModel {
        return ClassModel::whereHas('lectures', function($query) use($class_id, $lecture_id) {
            $query->where('class_id', $class_id);
            $query->where('lecture_id', $lecture_id);
        })->first();
    }
}
