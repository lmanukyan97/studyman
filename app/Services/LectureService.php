<?php

namespace App\Services;


use Illuminate\Support\Facades\DB;
use \Illuminate\Database\Eloquent\Collection;
use App\Exceptions\DuplicateException;
use App\Models\Lecture;
use App\Services\ClassService;

class LectureService
{

    protected $classService;

    public function __construct(ClassService $classService){
        $this->classService = $classService;
    }

    public function getAll() : Collection {
        return Lecture::get();
    }

    public function getById(int $id) : Lecture {
        $lectures = Lecture::findOrFail($id)->load('students');
        $student_ids = array_unique( $lectures->students->pluck('class_id')->toArray() );
        $lectures->classes = $student_ids ? $this->classService->getByIds($student_ids) : [];
        return $lectures;
    }

    public function create(array $data) : Lecture {
        return Lecture::create($data);
    }

    public function update(int $id, array $data) : Lecture {
        $student = Lecture::findOrFail($id);
        $student->update($data);
        return $student;
    }

    public function delete(int $id) : bool {
        $result = Lecture::findOrFail($id)->delete();
        return $result;
    }
}
