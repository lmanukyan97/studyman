<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ClassController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\LectureController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/classes', [ClassController::class, 'index']);
Route::get('/students', [StudentController::class, 'index']);
Route::get('/lectures', [LectureController::class, 'index']);

Route::get('/classes/{id}', [ClassController::class, 'show']);
Route::get('/students/{id}', [StudentController::class, 'show']);
Route::get('/lectures/{id}', [LectureController::class, 'show']);
Route::get('/classes/{id}/lectures', [ClassController::class, 'showLectures']);

Route::post('/classes', [ClassController::class, 'create']);
Route::post('/students', [StudentController::class, 'create']);
Route::post('/lectures', [LectureController::class, 'create']);
Route::post('/classes/lectures', [ClassController::class, 'addLecture']);
Route::post('/students/lectures', [StudentController::class, 'addLecture']);

Route::put('/classes/{id}', [ClassController::class, 'update']);
Route::put('/students/{id}', [StudentController::class, 'update']);
Route::put('/lectures/{id}', [LectureController::class, 'update']);
Route::put('/classes/lectures/{id}', [ClassController::class, 'updateLecture']);

Route::delete('/classes/{id}', [ClassController::class, 'delete']);
Route::delete('/students/{id}', [StudentController::class, 'delete']);
Route::delete('/lectures/{id}', [LectureController::class, 'delete']);
Route::delete('/classes/lectures/{id}', [ClassController::class, 'deleteLecture']);
